== 20160828 Kuren: The importance of mindfulness with your family


~~TRANSCRIPT~~

So there are many, many teachers through the centuries, which have come up with different ways to describe the different stages of enlightenment we go through.
One of the most famous is the Oxherding images that you see in Zen.
But there are also many others.

One of the simplest, and probably one of the [oldest], the descriptions that were used at the very beginning, the Buddha talked about, were four stages - so it makes it a bit simpler, only being four.
Daizan often likes to talk about these different stages.
So, stage number one is called "entering the stream."
And this really refers to that moment where you first have a taste of your true nature.
In Zen they call this kensho, which means seeing your true nature.
So at that point, you enter the stream.
You become part of the stream of people who know their true nature.

The second stage, and then the third stage, are kind of like different aspects of a similar process.
So, after you've seen your true nature, you then supposedly work on reducing the forces in your life that act as kind of push-pull factors.
The things that we draw to us, the things that we are attracted to, the things that we are averse to, things that we push away.
These are what people call "sense desires."
So that's stage two where we reduce these kinds of wanting this, not wanting that, wishing it was this way and not that way.
And also perhaps a reduction in our feelings of hate and anger towards other people and other things.
And then stage three would be a complete eradication of these forces of desire and aversion and anger.
So in a sense, maybe, stage two and three are a bit like a sliding scale, until we've dealt with all things on that level.
And then the fourth stage would be where we deal with things on the level of delusions - things we don't know we don't know, as one famous American politician put it!

So the way these stages work, mean that you can actually enter the stream, so touch in, find your true nature, see that you are not separate from the whole universe, you are one with the whole universe - and at that point, you may not have dealt at all with anything on the level of desire or aversion or hate or anger.
I suppose it's very unusual.
I suppose we're all dealing with that kind of stuff all the time.
But it is possible, nonetheless.
And I think it's fairly straightforward, quite an easy process, to actually touch in, find your true nature, have a kensho, as they say.
And we have these three day retreats that are specifically designed for finding for yourself.
But I think the next two stages - this reduction and eradication of the forces that pull us in one direction and push us in another direction - that takes quite a lot of work, and a lot of courage.

So how do we do that, then?
How do we reduce our desires and aversions?
I think it's also something important to notice, or to note, that whether or not we have touched in with our true nature, found that for ourself, it's always very important to work on this level of desire and aversion.
I think one of the most important skills or tools we have in this area is mindfulness.
And this has become very apparent to me in the last couple of weeks.
I've spent almost two weeks with my mum, just this last two weeks, and it's probably the longest time I've spent with my mum for years, in one go.
And the reason for this is because she was moving back - she's been living for ten years in Sweden - and she's moving back to the UK. So I went over and I spent three or four days with her in Stockholm, getting everything prepared, last-minute packing and all sorts, and then the removals men came and took all her stuff, and then myself, and her, and her two cats, we drove in her car back to England, and it took us three days.
So once we got back to the UK, in one piece, I then spent another five days or so with her in her new place, trying to get everything settled.

So my mum's in a wheelchair, and she's reasonably disabled, and I think, maybe, it's sort of like someone in a wheelchair is maybe a more extreme version of every person.
So, every person has things they can do, things they can do well, things that they can just about do, perhaps you would do it differently if you were to do it, and then things that they can't do.
So, obviously, my mum being in a wheelchair, there are certain things that she can definitely do, certain things that she can only do her way, and then certain things that she can't do.
So in these last few days I've felt this rising feeling, of wanting to help her.
And this is great, you know, this is fine, at the beginning - but then it starts to rise, and it starts to accelerate, the rising accelerates, I felt - to the point where I was putting aside the things that I know help me stay stable and balanced, in an effort to help her.
I was seeing all the things that needed to be done and all the things that she couldn't do, and I was wanting to do more and more.
And I think this is a trap that people get in to when they're caring for other people.
You perceive their need as being greater than yours, and so you put yourself to the side, and you sacrifice things that you know that nourish you, to help them.
And of course, that leaves you in a situation that is unbalanced.
And had this carried on, I can definitely see a situation where you start to run out of energy.

So watching this feeling arising, and watching this acceleration of this desire, this need, to do, to help, was definitely… - through my ability to be able to be aware, through the skill of mindfulness, I watched it arising.
And another thing that happens when you spend any time with family, is that family have the most incredible ability to press your buttons, to get underneath your skin and get you going, a bit.
So it's absolutely nothing to do with her, it's to do with me, of course.
She's just the way she is, and then I just get a bit irritated with certain things she was doing.
Which, of course, is fine - things she did irritated me.
That's all well and good.
You can just allow that arise, you acknowledge it, see it, and then of course you have a choice.
Once you see it, then you have a choice.
You can just allow it to be that way: notice your emotions, your sensations, and let them subside.
Or you can express: you can say something or do something or maybe even just leave the room.
And of course mindfulness, our ability to be aware of these things as they arise, is absolutely imperative in these situations.
If we can't notice, then we can't make a choice.
Everything is impulsive, or unconscious.

So then after a couple of days, and then a couple of days more, all these little things - habits and what have you - then you get to the point of being wound up.
And I was kind of just thinking about that word, you know, being "wound up."
We often think about that, we often say that.
So "winding up" has an implicit meaning of a cumulative effect, right?
So if someone does something, little tiny little thing, it's fine - once, is fine.
And then it happens again.
And maybe something else.
And then someone else does something in a similar situation, and then, you know, blah-blah-blah - and so it goes on, until you end up wound up.
And then the tiniest little thing, like a tiny little last straw, and then BOOF, you explode.

So mindfulness, again, is our skill here.
If we can be aware of things as they arise, and then make a conscious choice about what to do about them - either allow, feel, let them pass; or express, do something, in that moment - then there's nothing being held onto.
So of course that's how the winding up happens, right?
So it's not fully being acknowledged and expressed.
And then a little bit gets held on.
And then the next time something happens, it gets added onto that, and added on, added on, until you get wound up.
So then comes the skill of discerning, is it appropriate for me to simply acknowledge, allow, and let it pass, in this moment, whatever it is that's happened, or is it appropriate for me to say something, or do something?
And that takes practice, and it takes a little bit of time - and I definitely didn't get it right on many occasions this week.

But the real trick is to stay in the moment.
When we stay in the moment, then we can stay aware of the feelings, the sensations, and we can act on them.
It's very easy to get swept up into anxieties and worries about what might happen in the future, or whatever.
So we stay in the moment.
And then, of course, there's those situations where we're like, "oh, should I say something?
Should I say something now, or should I say something later?"
So again, it's that discernment about what is the appropriate action to take.
And in my experience, the more we can stay in the moment, the more we can stay in our feelings, in our sensations, our thoughts and emotions in the body in the moment, then the more obvious that natural course of action becomes.
So also mindfulness helps us to become aware of where we leak energy.
It takes a certain level of energy to maintain that kind of focus and concentration, that mindfulness, and also what we were doing, what I was helping my mum with this week, took a bit of energy, in terms of moving boxes and doing whatever needs to be done.
And of course there are times where you end up leaking a little bit.
Unnecessary worrying, or doing more than you need to, or whatever.

So the original translation that has become what we use as "mindfulness" now - the original word - was sati, in the Sanskrit.
And sati originally was translated as something like "remembering" or "recollecting", until it became more widely used as "mindfulness". So, remembering.
In those moments, we're remembering to stay rooted in the now, in the sensations and feelings now.
Remembering that I'm actually not separate from the world around, that there is no "I". The irritations, the annoyances, they often arise out of an underlying belief that actually I am separate, that I have something I need to protect here.
That there's a me that wants my way, and I do want this and I don't want that.
But, of course, if I remember that really I am not separate, that there is no separate self, in that sense, then that very much helps all those irritations to simply just… dissolve.

Another analogy that Daizan likes to use for this kind of enlightenment process, or realization process, that I think he often says comes from his teacher, Shinzan Roshi, in Japan - he talks about cleaning a window.
So we can have a window that's so dirty that it's black, we can't see anything through it at all.
And as we do our practice, it's a bit like cleaning the window.
And eventually a little shaft of light comes through, and that represents entering the stream, our first, seeing our true nature for the first time.
Seeing the light that comes through the window.
And as we continue to practice, it's really just about cleaning the rest of the window.
So that frames it (excuse the pun!) - it frames it in a very different way.
So we have entering the stream and then the sort of gradual dissolution of the forces that pull us this way and that way, OR we have simply just practice, where once we see the light, we just allow more of it to come through.
And as we practice, we bring more of ourselves into the light.
We see those moments where we get irritated or we get frustrated, it doesn't go "our" way, and then we accept, we acknowledge, we act from that place of consciousness and awareness.

So as a result, my relationship I think, I hope, over these years, has become a little bit easier with my mum.
I feel like we can have fun together, and I feel like there's a sense of flow or happiness in our relationship which definitely wasn't there before.
I feel like there's a bit more of a depth.
And I really do feel like it's much easier to love.
It's much easier to see the love in our relationship.
So I think practice is very worth it.

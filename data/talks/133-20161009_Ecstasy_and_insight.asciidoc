== 20161009 Ecstasy and insight


Summary: Referring to processes of letting go or gnosis that are particularly connected with energies of this time of year - autumn; Daizan introduces J.B. Yeats's two poems that are directly related to these experiences within the Zen practice.
Unfold they might in completely ordinary settings, these processes create a potential for something new that can come in, a sudden feeling of blessedness.
This comes and goes but through persisting in our practice we arrive to a point where a shift happens that changes our perspective of who we really are.
We see ourselves no longer as a separate entity in dualistic world but as a part of a process, a part of a flow that has no borders between - me and everything else.
Moreover, Daizan points out on dualistic base of our language that generally separates subject and object and the way that that reflects on our own reality; and on poetry as one of the way that language can be used to express ourselves non-dualistically.

~~TRANSCRIPTION~~

So in our Zen yoga practice this time of year we are very much exploring the process of letting go.
The energetic quality of the autumn season is very much connected with the lung and the large intestine energy channels or roots of energy within the body; and both of these organs are very important in the letting go process physiologically as well energetically.
This dimension of letting go, so prominent in the autumn season with the leaves dropping of the trees and so on, has got many different kind of levels of ramification.
I want to read you a very short poem.

"I sat, a solitary man,
In a crowded London shop,
An open book and empty cup
On the marble table-top.
While on the shop and street I gazed
My body of a sudden blazed;
And twenty minutes more or less
It seemed, so great my happiness,
That I was blessed and could bless."

This is a fragment of a longer poem by W.B. Yeats, considered to be one of the greatest English language poets of the 20th century, he won a Nobel prize and is very much considered a sort of an architect of Ireland as an independent country.
He was very involved with an Ireland culturally and also politically.
But he was also a Londoner, he had spent more than 30 years living in London; his house was up near the Euston Station; and this is very much a London poem.
He is sitting in a London café, perhaps he is in Starbucks or Café Nero -
"I sat, a solitary man,
In a crowded London shop,
An open book and empty cup
On the marble table-top." - so he has got a book with him, he has finished his tea and there is a kind of an openness.
"While on the shop and street I gazed
My body of a sudden blazed;
And twenty minutes more or less
It seemed, so great my happiness,
That I was blessed and could bless." - so he is in this kind of open empty state, he has emptied his cup, he has emptied actually his being.
A very important part of this letting go, that this time of year involves, is this emptying.
The technical terminology for it coming out of western culture is kenosis.
When we are able to let go of the old, when we are able to let go of how we were, we create a kind of a state of potential, sometimes called a spiritual pregnancy.
It is like there is an openness and something new can come in.
For Yeats he describes this new as - "..My body of a sudden blazed.." it's like his body suddenly caught a light and this experience lasted just "..twenty minutes more or less.
It seemed, so great my happiness, That I was blessed and could bless.." it is like a twenty-minute experience of being in heaven.
Everything absolutely sublime, absolutely perfect.
An ordinary London coffee shop, an ordinary London day, an ordinary London street, suddenly Yeats finds himself in heaven.
This is very common amongst meditators who are very good at letting go, at coming into this place of kenosis.
Some people tell me that practically every time they come down to the Dojo here, this is what it is like, their body catches a light, they experience being in this place where not only are they blessed but everything they look upon seems blessed.
And this is important in practice; some people have a lot of this but it is not central; it is important but not central.

It is interesting that Yeats himself a great poet has written about this experience in poetic form.
There is a very strong link between poetry and meditation practice that goes way back.
We have a collection of the Buddhist's own poems called Udana which you can read.
And probably of all approaches to meditation practice that exist in the world, the path of Zen has been one most closely associated with poetry.
Now why would this be?

Well, Zen is very concerned with our direct connection with our spiritual adequacy, with knowing and actually coming into a new being; in relationship with who we are.
Coming into a place in which we know that from the beginning we had all that we needed spiritually.
This shift of perception that Zen is very concerned with, is a nondual shift.
What I mean by that, is we are coming to a place of non-separation, the gap or the sense of split between me and the universe is healed, is mended, we don't any longer feel alienated or separated, a stranger in a strange land.
Now our language is dualistic - I know Camberwell - there is subject and object.
Our language is structured in a dualistic or separative way.
We distinguish many things with our language and this is very powerful and very important.
Our ability to use language essentially has remade this planet in our image.
It has brought us a tremendous benefit but there is a price tag.
When we use language in a dualistic way we think in a dualistic way and we can become hypnotised that this dualistic or separative mindset is the only one.
Is the only reality.
Poetry is one way in which human language can be used to point to a non-separative or non-dual kind of reality.

The heightening of the language, the quality of poetry within language is found within all cultures.
I don't think that there has been a culture yet, detected on a planet which doesn't have poetry as part of it.
Some people think that our own culture is very unpoetic, but actually it depends where you look.
The biggest selling rap record has sold 11 million albums, there are over 50 rap records that had sold over 3 million.
There is a vast audience for the human voice used with heightened language.

In terms of our Zen practice we have an important thread were we work with a question, a Koan as it is called.
The Koan that we study most often as a first one, is the question - Who am I? We use this question within the meditation to explore more and more deeply who we really are, to direct our attention towards the investigation of who we really are.
When we do this, there is a kind of a fairly predictable process that goes on.
Initially a lot of a common place, aspects of our experience, of our personal history, of our beliefs, of our sense of ourselves, all of these things come-up.
And if they come-up we let them go.
There is a gradual kenosis or emptying or letting go, because we see that these things are part of our history but not who we are.
As we go on, as our investigation goes deeper it is very common for, in a technical parlance - that the question goes colourless.
It becomes meaningless.
We almost don't know or don't care who we are.
It is like the question loses all meaning.
If we persist through this draining of meaning, this emptying, this kenosis continues to a certain point where a shift happens.
And this shift can be like Yeats described it - there can be this sense of the body of a sudden blazing, a kind of an exultation or a very ecstatic kind of period.
But as Yeats himself points out here, in his case it has lasted 20 minutes, this is something that comes and goes.
Sometimes these experiences of exultation come and go and that is it.
Sometimes there is a shift in perception and this shift doesn't come and doesn't go.
This is what Zen is really concerned with.
We have another little four-lined part of a poem from Yeats that expresses this dimension.
He has a little part that goes.
"O chestnut-tree, great-rooted blossomer,
Are you the leaf, the blossom or the bole?
O body swayed to music, O brightening glance,
How can we know the dancer from the dance?" - so the first two lines he is looking at a chestnut tree and he is examining it "..are you the leaf, the blossom or the bole?.." What are you really?
This is exactly parallel to this process of asking who am I really.
Whenever we examine anything in depth, in detail, this kenosis begins to happen, this opening, this emptying and then the perspective shifts to humanity.
"..O body swayed to music, O brightening glance,
How can we know the dancer from the dance?.." - there is no dancer separate to the dance.
You are a dancer.
You are a process not a thing.
When you actually discover and see and perceive directly that there is no thing that can point to who you are and yet this process dances on, this life continues, this shift in perception can't be un-shifted.
You know with every fibre of your being that you are the dance.
The dance of reality is you and is everything else, there is not you separate from this dance of reality.
You never were on this level separate from the dance of reality and so, you can relax.
You can be happy.
You don't have to defend anything, you don't have to become anything.
You can simply be what you are, the dance.
And you can enjoy your life fully.

So Yeats can very beautifully express what our practice is really geared towards.
He can express these moments, or this 20-minute period, or sometimes these two-week periods of exultation which will for some people come and go, but more importantly he can express very beautifully this shift in perspective that doesn't come, doesn't go. "..How can we know the dancer from the dance?"

So when you are sitting in meditation you are allowing this emptying, this letting go, this kenosis, you are allowing this to happen.
You are creating a condition of potential.
Out of this potential, if you stay with it, perhaps today, perhaps this evening, perhaps this week, it doesn't take very long necessarily; at a certain point this shift will be here for you.
You along with Yeats well be able to say - "..how can we know the dancer from the dance?"
Perhaps you too will feel that not only are you blessed, but you can bless, everything you see is blessed, is perfect, is beautiful as it is.
So in your meditation practice particularly this time of the year if you can allow this emptying, this letting go to happen; everything else comes from this.

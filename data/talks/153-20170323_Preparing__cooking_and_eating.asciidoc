== 20170323 Preparing, cooking and eating


Before we have a meal, and especially when we do our Zen breakfast here in the dojo, we do a particular set of chants and one of them is called The Five Reflections.
So, let me read them:

First, let us reflect deeply on our true efforts and the efforts of those who brought us this food.
Second, may we live in a way that makes us worthy to receive it.
Third, what is most important is the practice of mindfulness which helps us transcend greed, anger and delusions.
Fourth, we appreciate this food which sustains the good health of our body and mind.
Fifth, we accept this food to complete the awakening of the pure mind of all beings.

So the first of these reflections, reflecting deeply on the efforts of those who brought us this food... this is all in the area of the people who grew the food, transported the food, bought it, brought it here, and those who cooked it and prepared it.
In the temple setting, it is the job of the tenzo to do all of that.
Typically there'd be a head abbot, a chief monk, who's primarily concerned with the spiritual nurturing of his monks, and the second most senior guy would be the tenzo, the cook.
He'd be primarily concerned with the physical nurturing and nourishment of the monks.

The tenzo is a significant job.
There have been some interesting books written about the role of the tenzo in Zen literature.
I particularly want to look at this one.
It's called the Tenzo Kyokan or How to Cook Your Life.
Written by Zen Master Dogen in 1237 in Japan, just as Zen was really being brought into Japan.
Dogen travelled to China and studied with some Chinese monks, came back to Japan and looked at the temple and cook and thought it was a desperate situation, they weren't really applying themselves properly, and he wrote this book over a period of years.
He quotes a number of earlier works about this, particularly the Zen-en Shingi, which was written in China in the early 12th Century.

One of the first things he says in the book is "The Way-seeking mind of a tenzo is actualised by rolling up your sleeves". I think this is a wonderful way of putting it.
There's nothing high and mighty about that.
Roll up your sleeves and get on with it.
I'd like to read a couple of other quotes from this book.
He puts a great deal of emphasis on the mind of the tenzo as he's preparing the food.
He says he must "handle the ingredients as carefully as if he were handling his own eyes". He talks about "when washing the rice, don't lose even one grain". He emphasises that, it comes up a lot of times.
In those times, they would have had rice that would have been picked from the fields and mixed with bugs and grit and the tenzo would have had to wash the rice thoroughly....not even a single grain!

He also says, don't carelessly throw away the water you wash the rice in.
This applies to many things not just rice.
These days we see water as being this commodity that just comes out of the tap forever, but he's really emphasising not to even waste the water that you wash the rice with.
He says "see the pot as your own head...see the water as your lifeblood". "Handle even the single leaf of a green in such a way as it manifests the body of the Buddha". No matter what we're handling, what is being presented to us, see it as a jewel, as the body of the Buddha right there.
He goes on to say "great temples are built from ordinary greens". I love that phrase.

He also has a couple of recommendations for how to conduct yourself in the kitchen, making sure the pots and utensils are treated also in the same respect, as the body of the Buddha.
Making sure everything is put away, cleaned and ordered in the kitchen.
He also says "Under no circumstances, allow anyone who happens to be drifting through the kitchen to poke his fingers around or look into the pot". I think that may connect with a lot us.
There can often be people walking around the kitchen and interfering.
Keep them out.

He talks surprisingly little about what you actually do with the food.
It's not a book of recipes.
One of the few things he does say is to make sure the meal creates a harmony of the six flavours.
So we've got bitter, sour, sweet, salt, and he also includes mild and hot.
So creating a harmony...it doesn't have to be in equal amounts, because those six flavours are very much associated with seasons.
For example we're in the spring season which is very much associated with sourness, so maybe a little bit extra on the sour side is appropriate now.
But also we have the different colours as well.
I know in Japanese cooking they like to balance them.
White, blue/black, green, red, yellow.
Making sure the plate has different elements of those colours as well.
I'll read a little bit more...

"When making a soup with ordinary greens, don't be carried away by feelings of dislike towards them nor regard them lightly.
Neither jump for joy because you've been given ingredients of a superior quality to make a special dish.
By the same token, don't indulge in a meal because it's particularly good tasting.
There's no reason to feel an aversion to an ordinary one.
Don't be negligent and careless just because the materials seem plain and hesitate to work more diligently with materials of superior quality.
Your attitude towards things shouldn't be contingent upon their quality.
A person who is influenced by the quality of a thing, or who changes his speech and manner according to position or appearance of the person he meets, is not a man working of the Way".

There's a great deal there about how we conduct ourselves, how we handle the food, and the mind that we bring to the act of cooking.
Coming back to our Five Reflections, the second, third, fourth and fifth reflections refer to the state of being we are in when we eat the food.
It's equally important.
When we get presented with the food, it matters how we are when eating it.
It changes what it does to us, how we integrate it, depending on what our mind state is.

I have another book here written by a Zen teacher, Jan Chozen Bays, and she talks about mindful eating.
She's a teacher out in the US and she says we have 7 different kinds of hunger:
There's the hunger that arises from the eyes; we see something and it looks delicious.
Hunger that arises from your nose; we get that a lot if we walk past the bakery section in a supermarket or a coffee shop.
Hunger that arises from your mouth; wanting certain flavours or textures, salivation.
Hunger that arises from your stomach; the familiar gurgling of the stomach, feeling empty/full.
Hunger that arises from our selves; this would be something like a craving, "I really need something salty". Babies have this a lot and it's also a hunger associated with pregnancy.
Hunger that arises from your mind; the shoulds and shouldn'ts. "I should eat x amounts of calories a day, "I should eat less of this and more of that" etc."
Hunger that arises from your heart; this is comfort food.
It's definitely very there.
We can often feel hungry for something that makes us feel comforted.

She talks about bringing mindfulness to these different aspects.
It's quite difficult to do here as we haven't got any food here with us, but we can do a different kind of practice which we'll do here in a minute regarding feeling into the emptiness and fullness of different parts of the body.

The common thread between this, preparation and cooking, and eating, is "nari-kiru". It's a phrase that Daizan's teacher uses a lot. "Nari" means "to become", and "Kiru" means "cut off". To become cut off from all of the other things which distract us and just 100% be what we're doing, nothing else.
When we're cooking, we're just cooking.
When we're eating the food, we're just 100% eating the food.
Nothing else.
They say the nutritional value of the food increases when it's grown with love, cooked with love, and eaten with love.
These Five Reflections are, I think, very important.
Reflecting on the efforts of those who brought us the food, wishing that we live in a way that makes us worthy to receive it, practising mindfulness to become completely one with what we're doing, appreciating the fact that it's sustaining our physical and mental health, and that the food is contributing towards us waking up if we're living in the right way.

So how about we do a little practice tuning into the different feelings of emptiness or fullness in the body.
If you bring your attention first into your head, your physical head, your skull your jaw.
Does your head feel full?
This may be physical or mental.
Maybe your brain feels full!
Or can you feel sensations of emptiness?

Moving your attention to your chest, lungs and heart.
Does this area of your body feel full?
Are there any signals that are telling you it feels full or empty?
It might be difficult to discern and maybe those ideas don't really mean much here, but just noticing - do you feel any fullness or emptiness?

What about down in the belly?
Any feelings of fullness?
Any feelings of emptiness?
If you can feel one way or the other, what is it that's telling you that?
What are the sensations?

What about your legs?
Do they feel full or empty?
Maybe you've never thought about your legs like that before.

Or indeed, your arms.
Do they feel full or empty?

What about if we defocus a little bit and tune into the whole of the body.
The whole body together, does it feel full or empty?
Do you feel full or empty?

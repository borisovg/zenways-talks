== 20170319 Practising like you've got a fire on top of your head


~~TRANSCRIPTION~~

Often what happens when we start our meditation practice... "OK, today I'm going to be counting my breath", "OK, today I'm going to working with [whatever koan we've got]"..."OK, I establish my focus and concentration"...Five seconds later, "Hmm, what am I going to have for dinner tonight?
Oh no, hang on, I'm focused...focus....ah, you know that little ache in my left leg is....no, no, focus!
Focus..." So, this is often the way it goes.
It may be a conversation we had yesterday or something that we have to do tomorrow. "I must remember, I must remember to do that...".

The thoughts that arise and the different sensations in our practice are not a problem.
It's not a problem.
In fact, we're not trying to shut anything out.
If we try to shut anything out or block anything off then that is not so useful.
So, we allow.
When things get a bit difficult is when we really start getting into these thoughts and they take us off in these long chains and we're miles away from our point of focus.
And most of the time, it's involuntary.
Not many people sit down to meditate and then purposely start thinking of things, it just happens, it just comes up.
It takes us away from our experience of This.
The koan, breath, whatever it might be...open presence, Unborn meditation is all pointing to the experience of This right now.
I think sometimes one of the most important attitudes to bring to all that is simple acceptance. "OK, I've wandered off and got distracted again, that's fine.
I'll just come back.
No bad, no good.
I'll just practice a bit of kindness and come back to This".

But sometimes we've been sat there for 25 minutes and the bell goes and we suddenly realise "Gosh, I've been distracted for 95% of that 25 minutes". And you think perhaps you should have tried a bit harder.
And sometimes that is what we need: to just turn up the energy a little bit.
We need to apply ourselves in our practice just a little bit more, just put the focus on a bit more.
The Middle Path is not about being lazy, it's not about forcing it or pushing it either, but it's not about being lazy.
The Sanskrit word for not being lazy is "Virya", which translates into Japanese as "Shojin" or into English as something like "energy, perseverance, tenacity" and that type of thing.

There's a word in Japanese which you may have come across "Shojin Ryori" which means something like "Temple-style cooking". We had a teacher over a couple of years back who did a workshop with us about this. "Ryori" means "cuisine" and "Shojin" means something like "Right effort" or "Right focus". Virya is also one of the parameters.
We have six of these parameters that we cultivate for practice.

Looking at ourselves, at reality, takes effort.
It takes application of ourselves, it takes energy.
Most of the time, we're in the habit of not looking at This.
In fact, a lot of people spend a lot of energy trying not to look at This.
When they turn inside, it can be a bit chaotic, or uncomfortable, and a bit painful.
So it takes energy, effort, to turn in.
To look and to really sit with things as they are, whether that be pleasant or unpleasant.

Daizan uses the analogy of a window which is so dirty that you can't see through it.
Our practice, which might be a koan or whatever it might be, is like starting to scratch, to clean, a little bit of that window.
And of course, that takes energy, it takes effort to keep on cleaning.
And actually, cleaning a little corner and getting a first glimpse of light coming through doesn't take a huge amount of time; but for any of you who've been on one of our 3-day retreats or long retreats and have had that experience of the light coming through the window, it takes energy.
So then we have this little shaft of light coming through the window and if we don't keep working at it, it does have the potential of dirtying back up again.
But if we keep on cleaning, keep on cleaning, then of course we realize we have the rest of the window.
So moving from "kensho", the first taste of our true nature, to what might be called "full awakening" when we have the whole window cleaned, takes a lot of cleaning, a lot of Virya, energy, effort.

Zen Master Hakuin said "I encourage all of you seekers of the secret depths to devote yourself to penetrating and clarifying the Self as earnestly as you'd put out a fire on the top of your head.". I think "earnestly" isn't the word I'd use.
If I had a fire on the top of my head, I wouldn't "earnestly" put it out, I'd just damn well put it out as quickly as possible.
We need to practice like we have a fire on the top of our head.
Zen Master Obaku, from quite a lot further back in history- a Chinese guy - said "The Dharmakaya [the body of the Buddha's teaching] from ancient times until today, together with all the Buddhas and the patriarchs is One.
How can it lack a single hair of anything?
Even if you understand this, you must make the most strenuous efforts.
Throughout this life, you can never be certain of living long enough to take another breath".

So how long do you have left to live?
How many months do you have left?
I looked this up earlier: I'm 35, and if I live to the age of 80, I've got 540 months left of my life.
It's not that many.
But if you think about it in those terms, you can think "Oh God, I've only got 540 months left.
I'd better work hard, because if I don't work hard, I'll never get enlightened.
I'll never experience what it's like. 540 months...". But then, this is in the world of striving and pushing, forcing.
And where does that come from?
Maybe it comes from a sense of fear. "If I don't work hard enough, I won't get it in time". You might say that's wrong effort.
It's leading us towards more suffering.
It comes from a place of suffering.
It's motivated by self interest.
So how do we cultivate this sense of urgency without it being rooted in fear and self-motivation?
I think it's important to touch into why we practice.
Sure, we can practice because "I want to experience awakening" but at a deeper level we practice because we want to help.

So why does coming down here on a Sunday, sitting here for an hour looking within, why does that help other people?
The deeper we know ourselves, the more we can see our true nature and thus help, and that's because our true nature is as individual as a wave is on the ocean.
You look at a wave and you see there's a wave and that it's different to another wave.
But leave it a few minutes and you see that wave has come and gone and is actually just part of the same sea, right?
That's as individual as we are.
And when we start seeing that, it becomes much more clear why it's important to help are not different.
Other people and us are part of the same one Universe.
So when we practice with urgency because we want to help other people, because we want to ease suffering in the world, it's a different kind of motivation.

It's also important to realise that however many months of your life that you have left is only true in one perspective.
I have 540 months left of my life.
What is "my life"? What is "your life"? Past, future...these are all human concepts.
They help us understand the passage of events but they're really just ideas.
The life of Buddha is beginningless and endless.
This moment is beginningless and endless.
The world needs people who have woken up to this.
So when you come to practice and you establish your point of focus, your object of meditation, your intention, please practice as sincerely and deeply and with as much energy - Virya - and focus as you can.
It's as important as putting out a fire on top of your head.

== 20170312 The first sign of enlightenment


"Gratitude is the first sign of Enlightenment."
My first Zen teacher used to say this repeatedly and it came to mind yesterday.
I was teaching up in Nottingham and on the way down I was reading In Heaven's River, which is our translation of some of the poems of Enku from the 17th Century.
He was a very enlightened monk who lived in our area in Japan, the area where our home temple is, and Enku's poems... poem after poem have such a marked sense of gratitude and appreciation about them.

The reason I was looking at the Enku book yesterday was because yesterday was the anniversary of the tsunami when, 6 years ago in Japan, they had an earthquake that led not only to a tidal wave devastating a whole area of the country but the most severe nuclear accident that's happened in peacetime.
It's on the level of Chernobyl apparently, they were equally severe.
The book we brought out was a fundraiser for children.
Many children were orphaned in this tidal wave.
Also, Judith Charron who's here this afternoon has recorded an album setting some of these beautiful poems to music, and I hope you get a chance to hear it.
It's available on iTunes and all the other regular distribution outlets.
Both the book and the album are called In Heaven's River, which is a line from one of Enku's poems.

Now, why would gratitude be the first sign of Enlightenment?
When we come to turn our attention within, when we come to practice meditation, we're developing a greater familiarity with the reality - the conditions - of our existence.
As we come to see this more and more clearly, it becomes more and more obvious that our life, our existence, is a gift moment by moment.
It's not even really appropriate to talk about "my" life.
A very eminent physiologist once said he'd prefer to land an airliner full of passengers with no training than operate his own liver for even a few moments.
So many of the processes that actually engender our lives are completely out of our control.
Our very existence, moment by moment, is a gift.

It's a very common experience when this quality of sensitivity begins to develop in our internal examination.
It's very common to feel almost like we're being born moment by moment.
Actually not just like we're being born moment by moment, but the whole universe is arising moment by moment in this incredible gift... and it's not really "me" and "the universe". In this place, it's all coming together.

As we come to appreciate this moment by moment gift, what response can we make?
So far, as far as I've been able to see, the only possible alignment within this moment by moment gifting is to become generous ourselves.
I've often mentioned when I first arrived at the temple in Japan to study, just before I arrived the temple carpenter Morimoto-san - who was a simple uneducated Japanese carpenter who trained with his father and left school very young, a very ordinary person on many levels - had an enormous spiritual opening.
He was just flooded with love and generosity.
He was everywhere in the temple.
You went around a corner and he was chopping vegetables with the guys in the kitchen.
You went around the next corner and he was digging a hole preparing for somebody due to arrive in a wheelchair who needed a special pathway.
The next corner, he's mending the roof.
He was just everywhere.
His whole life became a sort of offering of love to other people.
Just being around him, this very ordinary man, was breathtaking.

The tightness, the lack of generosity that can come along when we feel under threat or closed or unenlightened is extremely unpleasant.
I'm sure we're all familiar with this.
When we're able to step outside of those walls, when we're able to appreciate this moment by moment gift of everything we see, everything we are, every dimension on every level, moment by moment presented to us in incredible splendour, when we do this and our eyes open in this direction, then this opening of generosity of spirit moves us from a closed and blocked condition to one of receiving and giving.
The image that we have in Enku's book and the recording as well is that of being in "Heaven's river". Not only being in Heaven's river...when we allow ourselves to step into this reality, we are Heaven's river.

Now this isn't some of kind of Pollyanna-ish attitude where we're closing our eyes to the fact that there's work that needs to be done, that this is a world of suffering.
Enku has a poem where he says:

Delight like eternal spring
Seeing even the suffering people as so many flowers.

Within him is this quality of springtime, of freshness, moment by moment arising.
Utterly delightful, utterly blissful.
Within this he can see that even the people suffering around him are moment by moment arising as flowers, but nevertheless he can still see their suffering, and he still has the motive to act.
Enku's life was incredibly dynamic.
He was an artist.
He carved 120,000 Buddhas and gave them away to the poorest people; many of them carved out of firewood that even the poorest person could spare.
The experts estimate that he carved probably 10 Buddhas a day and gave them away.
Not only that, but he lived in the mountains in the simplest conditions.
Some of his poems even talk about how he's living on the edge of starvation, and yet his heart is open and there's a quality of receiving, appreciation, gratitude.
There's this generosity that's just pouring through him.

We have, in one of the little museums dedicated to him in our area of Japan, his medical notes.
As he travelled around remote parts of Japan he operated as a kind of barefoot doctor.
He would help people suffering in the villages who were sick or needed particularly herbal treatments.
When we allow ourselves to appreciate what we're receiving moment by moment, the extraordinary gift moment by moment is presented to us.
This generosity that arises out of that is not any kind of diminution or any kind of problem to ourselves.
Shinzan Roshi, our teacher in Japan, likes to say that when we step into this reality we have a bank account that the more we spend, the more money we take out, the more it fills up.
We have one of Enku's poems...

In bliss
Autumn midnight
Carving
This world becomes
One full-moon circle

His carving isn't a job.
It isn't a task or even a mission.
It's just an expression of what it is to be receiving this life, this energy, this moment by moment gift.
So it's not problem to still be doing it at midnight.
And in this bliss that he's experiencing when he's carving, the whole world is included.
Enku's carving, his generosity, is just the universe being generous to the universe.

Now this all sounds perhaps a little bit exalted, a little bit distant from our daily reality, but it really isn't.
I'm a big believer in meditating every day and having a meditation diary, and after you do your 25 minutes sitting or whatever it is, just spend a few minutes writing in your diary.
For myself, what I tend to write first of all every morning, are the first 3 things that I'm grateful for.
Typically it's the most obvious things right there.
Most often I'll write things like "the air temperature", "the peace in the room", maybe "the carpet underneath me". Doesn't really matter.
All we need to do is just lightly turn the attention towards this quality of appreciation, even in a small way, and it just starts to build because it's real.
The more you can appreciate, the more alive you can become.
The more your life can go from block to flow, from closure to openness, from fear to your true magnificence.
You have a gift to give.
The universe is manifesting you for something.
The more you allow yourself to appreciate this moment by moment manifestation, the more you can put yourself in the way of what you're really here for.
As you do that, you can share with Enku in saying...."In bliss, even in midnight I can do my work.
This world becomes one full-moon circle".

If you don't have a copy of this book, we have a few here this afternoon.
I can sign them for you if you're interested.
All the profits, apart from the printing costs, go to these children and the charity Aid For Japan that we've been supporting.

== 20160925 Ordinary mind is the way


~~TRANSCRIPTION~~

My teacher's teacher in Japan was called Itsugai Roshi and this is a scroll of his calligraphy.
It is a very famous Zen saying that he has written here, that basically says - your ordinary mind (or your everyday mind) is the way.
It is a very ancient Zen saying and it has been widely misunderstood so I thought it would be useful to explore it a little bit.

It has been said that in this business that we are involved in, that you gain nothing out of this.
Why?
Because, actually, you have it already.
You actually already are fully spiritually adequate.
Now, that is a beautiful thing and true statement, but that is also a little bit like - suppose I said to you - actually you have a bank account with a million pounds in it, neither of us knows the bank account number nor which bank it is in, but it is there.
I suppose it might be nicer to know that, than not know that, but it is still not really going to help us that much.
If we actually know the bank account number and so on, then we can start spending; but not before.

Itsugai himself when he was a young monk, and he started as a boy, he was training in a very strict monastery in Kyoto in the old capital in Japan in a very traditional style of training.
In the early years of his training, him and his peers, they were very focused on actually getting the first foothold into the world of enlightenment.
They were very focused in terms of an image on finding out their bank account number so they could start spending.
Itsugai was the last of his intake, of the young monks who started the same time as him, was the last one to get it.
Considering he had been at this since he was a boy he felt a little bit ashamed that it was taking so long.
So he decided to take pretty drastic measures.
It was the beginning of winter and he decided he was not going to go to bed until he got it.
Every night at the end of the normal day of meditation and work and practice he would go out into the graveyard behind the temple and he would meditate all night.
He would sit in the snow.
Sometimes the snow would pile-up on his body and he would just meditate and meditate.
He pushed himself really hard.
In the morning sometimes when he came in to do his interview with his own teacher, he would literally faint from the cold.
But he pushed and pushed and pushed.
This went on right through the winter.
Still he could not find what he was looking for.
He still could not get the switch.
He could not get the foothold in the world of enlightenment.

It got to the day before a week of meditation, a Sesshin retreat.
The other young monks who already got it would all went off for, it is normally a rest day a day before a retreat, and they went off into town to do a bit of a shopping and relaxing and so on.
Itsugai stayed in the monastery and carried on meditating.
Earnestly seeking for what he was looking for.
As the day wore on he realised everybody had done their laundry and their laundry was dry but they were not back yet to take it in.
So he took everybody's laundry in, folded it up, put in on their beds ready for them and so on; still no sign of the other monks.
So the other major thing that happens on the rest days, is a bath time.
The bath in Japan is one bath for everybody.
The water gets heated-up and you clean before you get in, so the bath can actually cover everybody.
So he started to prepare the bath.
It was the old style bath with wood and twigs underneath it to heat the water.
He started to make a little pile of wood and he lit it.
Then he started to pile more wood in to get the water really hot.
He was very focused in his meditation and he piled in actually more wood that it really needed.

Suddenly, completely unexpectedly a jet of fire shot out from underneath the bath.
Straight at him.
And the shock from this little jet of fire suddenly shifted everything.
He got what he was looking for.
When the other monks came back from their day out they found him literally dancing around the monastery.
And here we are, many years later, the abbot of the very big and prestigious Zen monastery writing this phrase - your ordinary mind or your everyday mind is the way; he would not have been able to write this unless he has been through that early shift of awareness, to write it with full authenticity, to really see and know that you actually have this bank account full of money right now, it does not mean anything until you know the bank account numbers and you actually start using it.

So when we come to practice, our first job is to get this shift of awareness where we can genuinely see and know that we had it all from the beginning.
We always will, nobody can take this away from you nobody can give it to you.
It is yours right now.
How do we get the shift?
Well, what stops us from getting the shift is our sense of separation, our sense of isolation or alienation, our sense of me versus the world.
This manifests in many different ways, but essentially what we do is we lock ourselves up, we hide ourselves behind walls.

Rather than hiding behind walls our first job is to come out from behind the defences, to take the risk that we don't need to protect or defend anything.
One of the reasons we have a place like the Dojo here is, it is a safe place where you can explore what it might mean to actually step out from behind your defensive walls.
Because the truth of the matter is, when you stop holding yourself separate you automatically find yourself to be one, to be at home in the universe.
And you always were.
Like a fish is always one with the ocean.
The fish might feel separate but the fish is never separate.
You also are one with the great ocean.
What makes you feel separate is a sense of holding-on a sense of clinging.

Our process to actually reverse this is simply the process of letting go.
Right now, perhaps you can let go in your body.
Perhaps you can let go in your emotions.
What I mean by that, is simply let them come and let them go without any resistance, without any holding or trying to grab anything.
Perhaps you can let your thoughts come and go.
Perhaps you can move from a sense of clench or tightness, a sense of resistance to simply opening your hearts, allowing life to flow through you.
When we do this in our meditation there is no longer any internal or external.
When we let go right now, I could ask you a question - whether the sound of the aeroplane flying over is happening inside you or outside you.
And actually the question becomes meaningless.
If it is inside you, everything is inside you.
If it is outside you, everything is outside you.
It is nothing you need to fight, nothing you need to resist, nothing you need to loose, nothing you need to gain.
We have mentioned before, the Buddha himself expresses this place as - entering the stream.
Allowing yourself to simply come home, allowing the universe to flow through you, allowing yourself to flow through the whole universe.
Nothing to hold-on to, nothing to push away.
Your ordinary everyday mind is the way.
Your ordinary everyday body is the way.
You don't have to be any more clever that you are right now.
Or any more intelligent, or any more holly, or any more developed as a human being.
This is it.

Now when we let go into this place, into the way, there is no end point.
We don't stop.
Nothing stops.
Everything continues but our perspective shifts.
Experiences of all kinds come and go.
Thoughts, memories, perceptions come and go.
But in the same way, in your growth process going from a child to an adult, at the certain point you realised that father Christmas didn't exist.
And most likely, almost certainly, for the rest of your life you are never going to start believing in father Christmas again.
In a same way, there is a certain threshold that comes in your practice when you allow yourself to enter this stream where you can see that there really is nothing you need to defend.

This action of resistance and defensiveness is a little bit like an allergy.
Our body can become allergic to something that is of no harm to it at all, like grass pollen; and then resist strongly grass pollen which has no health effects of any kind.
And so a massive reaction can be set up to something that really has no threat to us.
In a same way in our growth we can almost become allergic to the universe, more and more defensive, closed and resistant.
But it is all based on nothing.
There is nothing you need to loose, nothing you need to gain.
All you need to do to cross this threshold is to allow yourself to enter this stream fully.
Allowing your everyday mind, your everyday body to be the way.
Not resisting anything, not holding on to anything, allowing yourself to enter the stream.
Allowing yourself to find, for yourself, this bank account number so that you know for yourself the untold riches that are yours and you can begin spending.

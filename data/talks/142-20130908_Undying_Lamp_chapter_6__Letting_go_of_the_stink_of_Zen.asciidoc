== Chapter 6: Letting Go of the Stink of Zen


For a while now we have been studying the book by a great Zen master called Torei Zenji.
Its English name is “The Discourse on the Inexhaustible Lamp”, and it is the training manual for our approach to Zen.
It has ten chapters, and we are looking at chapter six today, which is titled "Advanced Practice". Master Torei has a sequence running.
In the fourth chapter, he makes the point very strongly that it is absolutely critical in Zen practice that we have an initial, clear-cut understanding into what the Buddhists and ancestors were pointing to.
He makes it abundantly clear that it is absolutely essential that we experience what we call in Zen "kensho" - a direct perception of, or direct connection with, your true nature.
In the fifth chapter, titled "Passing Through Barriers", Master Torei goes on to say that that is not enough.
It is not enough to have an initial understanding.
We need to deepen that understanding, through studying further koans or further spiritual problems, so that the initial understanding is deepened and made more all-encompassing.

The chapter we are going to study now is called "Advanced Practice”, and he pursues his theme again.
The one way of advanced practice is letting go of the body.
This is called the one salient point that the ancestors cannot transmit.
Master Hoseki of Hanzan says, “A thousand stages cannot transmit the one way of advanced practice.
Trainees who play with forms only are just like monkeys grabbing at reflections.
It is also called the final phrase".

So what is this advanced practice?
Torei Zenji actually piles up many prior cases of people pursuing this advanced practice, to outline what it is.
There is one, in particular, that I think lays out the way very clearly.
He describes the progression of Master Goso Hoen.
He says Master Goso Hoen first trained under Master Ensho, where he came to understand all the affinity links or koans of past and present.
Then he went on to Master En of Isan mountain who said the World-Honoured One had a secret word.
His raising of the flower is this secret word, and Kasyapa's smile was his total response to it. In truth, he hid nothing away.
Goso clearly understood this and his doubt was dissolved.
After that, Goso went to train under Hakuin.
One day, when Hakuin had ascended the high seat in the Dharma hall, Goso raised the case of a monk and asked about the Mani jewel and asked for an explanation.
Hakuin scolded Goso, who at these scoldings started pouring with sweat and obtained satori or kensho.

Not long after that, Goso was appointed overseer of the rice pounding.
Later Hakuin came into the shed and said to Goso, “Have you heard what happened?”
Goso said, “No, I haven’t”.
Hakuin told him that a few days earlier he had some Zen guests that came from Rosan (an important training mountain). All of them had attained satori or kensho, and he made them expound on the teaching and they did it faultlessly.
They were also well-versed in the koans, and they understood what Hakuin said.
Goso asked, “And what about you, Osho (teacher)?". Hakuin replied, "I said to them it is not yet there". Hearing this, a great doubt arose in Goso considering his own state. He wondered whether he had already completed satori or kensho. "I can teach, and as for elucidating, I can do that also.
Why, then, is it not yet there?"
He applied himself with all his might, continuing for many days, and finally forgot about eating and drinking.
After seven days, he truly arrived at understanding the truth.
In one go, he threw away the precious jewels of bygone days and ran to see Hakuin, who showed himself so overcome that he did not move either hand or foot, and Goso could only smile.

Master Torei goes on to say that Goso had first clearly understood the World-Honoured One's secret word.
Then, on being harshly scolded by Hakuin, he had thoroughly attained the great joy, but coming up against this "Not Yet There" koan, he had again great doubt.
So great that he truly broke his own bones over it, forgetting eating and drinking.
After seven days, the doubt at last passed and he saw the meaning.
In one go, he threw away all the precious jewels and treasures of bygone days and all that he had come to know until then.
All the kenshos and insights he had obtained into the Buddha and the Dharma shattered, all at once.
Having broken his bones to such an extent, the joy he felt was all the greater.
He burst into Hakuin's room, who seeing him thus was himself overjoyed and indicated it by showing himself so overcome that he could not move.
Goso also could do no more than smile his joy.
When later instructing his own trainees, Goso liked to refer to this, saying, “When first I reached there, white sweat just poured down from me. From then on, it is like unloading the cargo, the clear wind blows”.

As we develop in our practice, as we practice sincerely and assiduously, very quickly like Master Goso we can have a direct understanding of the truth.
We can come to know who we really are.
Then, we have a period of seasoning, where we actually explore further understandings, further kenshos.
So we have Goso, who studied the koan of the World-Honoured One's secret word.
Another koan about the mind jewel is another important koan, and through doing this his understanding grew deeper.
His experience grew more all-encompassing.

What is this something-further that is so important that Master Torei devotes a whole chapter of his ten-chapter work to pointing it out?
When we take the road of practice, as our understanding deepens, opens and develops, the transformation that occurs is all-encompassing and actually has a very powerful effect on us and changes us.
But there is something left over.
Shinzan Roshi often refers to this little bit that is left over as “the stink of Zen”.
When somebody develops in their practice, they can have a stink of Zen about them.
There is a trace of self-consciousness about their understanding, their attainment.
Even this understanding, this attainment itself, has to go, for things to mature sufficiently.
In the Christian tradition, we have Meister Eckhart, the great German mystic, talking about reaching the point where you have nothing, want nothing and know nothing.
T.S. Eliot, the poet, talks about a condition of complete simplicity, costing not less than everything.
Master Goso likes to use this term, for his monks, about this place, where he says, “Unloading the cargo, the clear wind blows”. When we completely let go, even of the understanding itself, then at last a clear wind can blow.
When we are holding on even to a trace of our understanding, then there is still further to go.

Even the understanding itself goes, so what are we left with?
I remember Shinzan Roshi one time in Japan saying, we go through this difficult training and we end up, saying it in English, “a good man everybody like”. That is all that is left over.
An image I've sometimes used previously is that this first understanding is like a first taste of a delicious banquet, and then the progressing through the barriers that Master Torei talked about in the previous chapter is like actually eating all of the banquet.
But the process isn't finished there, until not only have we eaten the banquet but we have also digested the banquet.
The banquet in a sense, has become us and in that process of becoming us, the banquet disappears, in the same way our understanding disappears.

How do we do this?
How do we get to this point?
Certain people in the Zen world aim for this place from the outset.
They say, “Don't worry about getting any kind of understanding; just go here and let go.
Just have nothing, want nothing, know nothing.
Just be in this non-gaining place from the beginning”.
So what is the problem with that?
Why would Master Torei emphasise the two previous places before?
It is very easy to overlook the fact that Zen practice is a transformative practice.
We gain nothing, in a sense, but, in another sense, eating this banquet has tremendous influence upon us and, in eating this banquet, we become able to guide others, in eating this beautiful banquet that is available.
I do not recommend lingering in any of these places, but I also don't recommend missing any of these places out or attempting to bypass any of these places.
There really are no shortcuts.
We can go fast, we can go slowly - but, from my experience and as far as I understand, there are really no shortcuts in this process.
We can't avoid any steps along the way.
As we go on, our understanding will open and develop.

How do we deal with this last piece, this stink of Zen?
How do we let go, finally?
I think we can adapt an image that the Buddha used in his early teachings.
He talked about making fire, and using a stick as a kind of a poker to develop the blaze and keep the blaze going.
As the stick is used to turn over other sticks and keep the fire going, the poker stick we are using itself burns down, and eventually is consumed in the fire.
In the same way, this last piece of self-consciousness that we have left over, as we go through this process, also will be consumed or burned up, if we just keep going.
If we are aware that it is possible to get caught, to end up like the monks from Rosan - the monks who all have deep understanding, who can all explain the Dharma clearly and truly, but nevertheless are not quite there - if we understand that we let go even of our understanding, then this process can carry on.

You may very likely feel that, as we let go of our understanding, we may be left with nothing.
And the truth is we are left with nothing.
Fortunately, we need nothing, and as we end up in this condition of nothingness, then, as Master Goso points out, the pure wind can blow.
As we unload all the cargo, pure wind can blow through, and the pure wind is our true life.
You can never hold on to it; you can never trap, store or keep it; but it is intimately yours.
But you can only fully know it, and embody it, and live it through unloading the cargo.
So, we go deeper and deeper; we expand our understanding as deeply as we can.
Every step of the way, we are unloading more of the cargo, until there is really nothing left.
As we do this, we can almost feel like we are betraying everything that we have perhaps lived for, everything that we've made a central plan within our lives.
But in letting go of the image of Zen that we are holding on to, we step into the true place of Zen.

As you do this your practice may take on many different forms.
It may well take on forms that aren't necessarily in the common conception of things considered anything particularly connected with Zen.
The pure breeze that blows has its own intentions, its own imperatives.
And as we unload the cargo, we open ourselves to these possibilities.
As we step forward, we are necessarily stepping forward into the unknown.
We have nothing; we know nothing; and yet life goes on in abundance.
This is the road of, perhaps, the last great adventure that human beings can have on this planet.
All the other adventures are already filmed and put on the telly.
With this one, every step of the way is a step into the unknown.
Our part to play is to unload the cargo, and allow the pure wind to blow freely.
 
_In a previous chapter we talked about faith._
_How does our faith unfold as part of this process?_

Faith is often thought about in terms of belief, but I think in the context in which we are exploring it here, faith is actually what you do.
If you get up in the morning and get on your cushion, that is faith right there, in a nutshell.
Faith is being willing to face whatever arises - those physical clenches and emotional releases.
Sometimes, a sense arises of not knowing where it is all going - and actually facing these things and keeping going - that is faith.
That is it.
There is not really a particular emotional quality or intellectual quality that really embodies faith.

So, in that sense, faith is best cultivated by cultivating habits, beneficial habits.
In every level of working with, for example, spiritual practice and meditation practice, there is tremendous benefit in building up the habit of practising in the good times in your life - so that when things are more difficult or more troubled in your life, or perhaps even busy, you have this good habit and that habit energy is already established.
That way you are much more likely to stay with your practice, and therefore much more likely to ride out the bumpy times most gracefully.
Faith, in that sense, is that habit energy that we build.
We build it through getting up and doing our practice.
Sometimes it seems like there is nothing going on; sometimes it seems like we are just going through the motions.
Sometimes all hell is breaking loose.
Sometimes it’s bliss and beauty - it doesn't matter.
In a sense, the faith is actually keeping going regardless.
 
_How do you unload the cargo? Is this something you can do practically in your daily life?_

Keeping with unloading the cargo is to actually see the cargo.
To actually see. We actually hold on to the cargo.
We carry the cargo around. It’s like we carry this rucksack of stuff around - memories, senses of how the world is. All this stuff we carry about.
If we can see it clearly enough, then just that seeing itself goes about 95 percent of the way towards actually letting it go.
Because the more clearly we see it, the more useless we actually see it is - the less helpful it actually seems to be.
It is all primarily about that clear seeing.

So where do we see the cargo?
The Buddha himself pointed out these four dimensions where it is clearest.
The actual physicality of your body - the structure of your body.
For example, somebody who has an habitual attitude of being closed against life tends to walk around like this - literally built into the structure of their body.
And the more we perceive that physical structure, that is embodying this cargo and that is being carried, strangely enough the more it tends to correct itself, the more it tends to start to release, and open the body and the sensations - the actual feelings within the body.
For example, a lot of grief tends to get locked into the lungs.
So we can actually explore feeling our way into the lungs, into the energy meridians connected with the lungs, and releasing that area - and through doing that, the cargo is released.
We can explore on the level of the mind - the concepts, the world views that we carry around.
The more we see them clearly, the more we tend to release them.

Whichever dimension you wish or is most vivid for you, you can actively work on releasing your grip on this cargo, because any one of those will deal with all of it.
You can deal with the body structure, and that will release the energy.
You can deal with the energetic stuff, and that will release the body and completely change the mind.
You can work on the mind, and that will sort out the body.
It doesn't really matter.
Just work on any one of the levels, or whichever is most vivid or most clear for you, and you will sort out all of them.
The human organism is one integrated system.
When you loosen off one strand out of a rope, then the whole rope becomes unbound.
It works exactly the same way.
Cultivating this awareness is the key thing - this presence of mind - and studying the body, the mind and the phenomena within the body and the mind.
 
_There is a strong sense of progression within Zen practice, and yet at the same time the potential for immediate enlightenment at any point.
How can these two ideas be reconciled?_

When we do a measure of Zen practice, when we have got into the groove of it, it is not like a sort of spiritual painkiller or morphine. It doesn't stop us feeling.
So what does it do?
People don't get knocked off their perches so much.
So what is different?
It is not that the feelings don't arise, but what is different?
It is a life skill that you develop and you get better at it.
But you still fall over and so on.
We become much more able to let things arise and let them pass, without buying into them in the same way.
We are not so invested in every thought that goes by or every feeling that goes by.
And when it is not a particularly pleasant feeling, we therefore conclude, “Okay, I'm an awful person”, or “I am not good enough”, or whatever - we don't do that any more.
Or we do it much less.
And so, life does take on a greater quality of equanimity. Stuff can arise; stuff can pass away - good stuff, bad stuff.
We stop buying into things in the same way.
This is a tremendously valuable skill.
It is good for you, in a sense that you stop harming yourself so much internally.
But it also very important, in terms of your question.

Let me see if I can boil down your question: on one level, we have this sense of progression here; there is a road of practice.
From another perspective, we actually touch all of this potentiality every moment.
How can these two levels both be true?
On the level of the every-moment potential, the earliest Buddhist teachings have a term, “thaddanga nirvana”, which means “momentary nirvana”, “nirvana in the moment”.
When, in the moment, we actually are able to let go, we experience immediately what the Buddha is pointing towards.
Using the example of a stiff body, right now my body is stiff; my neck feels out of line.
Perhaps my feet are falling asleep, because we have been sat here for a bit of time. Where is the thaddanga nirvana?
Where can I actually find this liberation within this actual moment, within this less-than-ideal physical experience?

As long as we have, essentially, “me-and-my-body”, we will never touch this momentary enlightenment.
When the gap between “me” and “my body” is healed - when there is no separation, when there is no gap - we experience everything: bliss, beauty, everything that we care to name. All the release and liberation is available for you within this very moment.
When we become one with one thing, we become one with everything.
When we become one with one thing, we become one with the whole universe - and in that there is a completion; there is a perfection; there is a bliss; there is truth.
And you can experience it right now.
You can experience what the Buddha is pointing towards, with a stiff body, painful knees.
You keep falling asleep and your shoulders feel a bit funny.
When you don't resist any of that, when you don't try to change any of that, when you are one with that physical situation right now - there you are. That's it - right now, right here.
You can do this on a physical level; you can do it on the level of all these thoughts floating about; on the level of emotion - it doesn't matter.
In Zen, we use this term, “nari kiru”, to refer to this process of becoming the situation, or the koan, or the example of enlightenment that is right in front of us.
We don't have to wait for a physical perfection.
We don't have to wait for our minds to be emptied.
We don't have to wait for anything.
It is all findable right now, in this moment.
This thaddanga nirvana, this momentary nirvana, is here right now.
 
If that is true then what is all this about progression, and initial understandings and going deeper, and cargoes and all that stuff?
How does that fit in?
How does that square with this?
One of my teachers used to use the image of a window.
If you imagine a very dirty window, we get our cleaning cloth and we start at one corner, and we scrub away at that corner of the window.
When we do that, eventually the light starts to come in.
So now we have the light coming in through the window.
It is only one corner, but, nevertheless, there is the light.
That light is the light.
That's it.
We've got it.
Now, if we want to carry on, we can take another corner, and another corner, etc., and gradually, progressively, we can clean the whole window.
Now, the experience of having the whole window cleaned is different from the experience of having just a little corner cleaned and the light coming in.
Nevertheless, it is the same light - but, at the same time, it is different.
So the progression dimension of practice is this process of cleaning the window more and more thoroughly.
But, in a sense, right at the beginning, in the moment - stiff body, right now - we get it all.

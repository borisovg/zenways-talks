== 20140720 Washing the bowl


Around 1200 years ago, there was a very highly regarded Zen Master who didn't start teaching until he was 80 years old, and he lived till he was 120. His name was Joshu and he was very famous for his teaching with words.
His style of teaching was called "lip and mouth" Zen.
He was very clever with words and he could open up the truth for people using words very successfully.
One day, a new monk came to him and said "I'm a new monk in the temple.
Can you give me some teaching please?"
Master Joshu asked "Have you eaten?", the new monk said "Yes I have", and then Joshu answered "Then wash your bowl". The simplest thing - you eat your meal and then wash your bowl.

There are lots of ways that this very simple little interchange can be looked at it.
One way is from a symbolic point of view.
In Zen, the mealtime and act of eating has a kind of symbolism.
Your bowl, when you eat - usually your main dish is in your bowl - and the bowl represents you.
You and the bowl are the same thing.
So when you've got your food, your bowl is filled.
When we go through life, when we experience the process of growing and developing as human beings, our bowls naturally get filled.
They get filled with experiences from the past, world views, with a sense of who we are and how the world and other people are, with opinions, and sometimes suffering, burdens and things that we're carrying around.

If our bowl overfills, gets too much, we can have problems in life.
We can get sick.
If someone is carrying too much, on a physical level or psychological level, they get ill.
The burden is too great.
Most of us don't get to that point where we get that sick, but we can get to the point that we're carrying so much around that we don't have much room for anything new in life.
So, an important part of practice is to learn what it means to begin the process of emptying our bowls, to learn to eat, as it were, within our bowls.
When we do this in the right way, the stuff that was in our bowls can actually nourish us.
That which causes pain, suffering, and causes us to feel blocked and closed, when it's treated right, actually nourishes us, makes us stronger, and makes us more able to do what we're here to do.

An important part of practice is learning how to digest that which is in our bowls; and when we do this, we can get to the point where we begin to empty our bowls, and we come to Master Joshu and we ask what to do next, and he says "Wash your bowl". In the Zen mealtime ceremony, which we've done many times here and many of you are familiar with, we wash the bowl using tea.
So, once the bowl is empty, tea goes in the bowl, we wash the bowl using tea and then we drink that as well.
The tea represents what in Japanese is called "kanro". Kanro is very similar to what we could call "ambrosia" - the nectar, the food of the gods.
There's a kind of blissful, sweet flow that we experience when we empty our bowls, when we make enough space within our body and mind for things to begin to move and come alive.
The tea represents this blissful nectar.
The tea washes out any last aspects of this closure or blockage, and then we drink this as as well.
We eat our meal, we wash our bowl.
This process of purifying the body and mind is very central within our practice.

Shinzan Roshi, our teacher in Japan, told me years ago how when we he was a young man and had left university, he had great plans to be a business tycoon.
He wanted to build a business empire.
He invested all the money that he had, and all the money his parents had, into a couple of businesses that he was developing.
He had great hopes for these businesses.
But he wasn't the most successful businessman going and he managed to lose, not only all of his own money, but all of his parents money as well.
He went bust, and feeling like a failure, feeling despondent, he attempted suicide - and he managed to even fail at that.
In this place where he felt like a completely useless being, like his whole life was a failure and worthless, he was driving past a railway station and saw a nun waiting for a taxi.
He pulled up and offered her a lift, and took her to her home temple.
This nun was very cheerful, positive and kind, and had a real brightness and energy about her.
He had no interest in religion previously, but something about this nun really struck him, and he began to ask her about life and explained to her about what he'd experienced and his feelings of being useless and a failure.

She wouldn't actually answer many of his questions.
She didn't pretend to be any kind of a teacher, but she gave him a book.
The book was called "Shinsen-roku" or "The Record of Washing the Heart". Shinzan Roshi took this book away and it took a little time because he was pretty anti-religion in his young life, but eventually he found himself reading this book and it completely spoke to him.
Suddenly he could see a point, a purpose within life.
He went back to this nun and talked more with her and also met some of her fellow nuns, Zen nuns from the Rinzai Zen school, and they were very kind to this lost young man.
Eventually he decided that he wanted to enter a monastery and practice full time, and the nuns were quite fond of him by this point and they made his robes for him and got him into the local Zen training monastery.
His career as a Zen practitioner really started from this point.

This book that was so important to him - "The Record of Washing the Heart" - came to mind recently because when we in Japan a couple of weeks ago, we celebrated Shinzan Roshi's 80th birthday.
When you're born in Japan, you're counted as aged 1, so in Europe it would actually be his 79th birthday, but in Japan, it was his 80th.
A big number like that, of course, causes one to look back, and I think he was looking back over that half century or so of his Zen practice.
One of the things that happened while we were over in Japan was I proposed to Shinzan Roshi that we celebrate his European 80th birthday in 12 months time over here in London, and I suggested an art exhibition as well as a number of events.
He's quite a well known Zen calligrapher.
He agreed and has created a series of Zen calligraphies that we're going to exhibit as part of this exhibition.

There's one in particular that I wanted to show you.
He's done a series of this one here - [*lifts calligraphy*]. This character here is the character for "heart" or "mind", and this is the character for "wash". So he's thinking back to the Shinsen-roku, the book that changed his life, nearly 50 years ago.
He's done a series of these, among others, and they've come over to Europe to be photographed and then they're going back to Japan to be turned into hanging scrolls.
I wanted to show you this.
This aspect of purifying the heart, emptying the bowl and allowing the blissful nectar to wash through us, I wanted to bring this aspect of practice to our attention as it's obviously something on Shinzan Roshi's mind at the moment.

Now, how does this process go on, this purification go on?
Well, what we learn to do is, firstly we learn how to let go.
Secondly, we learn how not pick things up.
When we come to practice for the first time, in my experience, people come mostly for two reasons.
There are people who come to practice because they've had a lot of success in their lives.
They've achieved what they wanted to achieve in their lives and there is the inevitable sense of "What now?
That can't be it.
Surely there's more!". And then other people come to practice because they have a sense that "Well, life has been not so brilliant so far.
Surely there is more, a better way to live".

So, in a sense, both groups come with a feeling of potential - "There must be more to find; more happiness, more joy, more peace in my life". When we act on this sense of "There must be more" a kind of invisible door opens.
When we step through this invisible door, we're brought face to face with ourselves.
When we genuinely come face to face with ourselves, we have a choice.
Most people, to be really honest, when they come to this point, they run away.
They avoid genuinely facing themselves.
A few people keep on looking.
It takes a lot of courage, a lot of commitment.
Sometimes it even takes a level of desperation.
When we continue to face ourselves without avoiding the issue, without looking elsewhere, then we start to see more and more clearly how we live out of harmony with who we really are and how life really is.
If we continue facing ourselves in our meditation, in what we do without lives, then we begin to see more and more ramifications.
More and more aspects in which our life has been out of harmony with who we really are, with what we're really about.

How does this out of harmony aspect manifest?
It manifests as physical tensions in the body, emotional blockages...in many different ways, but typically all in the area of closure or block.
This sense of restriction, of not being fully who we could be, on whatever level that might be.
When we keep looking, when we keep facing all this stuff, something amazing happens.
Our attention, our awareness itself, causes these blocks to begin to melt, to dissolve, to release.
In this attention, in the field of our awareness, we start to let go.
We start to release.
We can do this in our sitting meditation but we can also do this in the way that we live.
We start to see more and more clearly what we do that's out of harmony.
We see it because it leaves traces.
So as we do this, our life automatically begins to come back into harmony with who we are, who we really are.

From the external point of view, in terms of our behaviour and actions within life, we have the Zen precepts, the code of behaviour which offers a kind of description of a life that is in harmony with our true nature.
As we practice more, as we face ourselves more, automatically we start to live a more upright life, a more genuine life, because we see clearly how it is when we don't, what gets left behind when we don't.
In terms of our inner work, our sitting meditation, we come to face ourselves every day.
We practice even for ten or twenty minutes every day, and we start to see more and more clearly....typically the gross ways in which our life has been out of tune...but there's a kind of refinement that goes on.
We start to bring our lives into tune more and more.
It happens by itself just through our courageous awareness, our willingness to keep looking.
As we do this, we digest what's in our bowl.
We create more and more space in our lives.
We create more and more freedom in our lives.
Those things that tended to close us down and makes us feel imprisoned start to not work any more.
We become more and more free, and a moment comes when we directly experience the blissful flow.
We move from digesting our meal, to the tea.
We start to wash our bowl in a whole new way.
And the refinement continues still.
We become more and more in tune with what we're here to do.
Our life becomes, in a sense, a force of nature.
We become able to surrender all our thoughts and ideas and opinions about what our life should be, and we become able to allow our life to take the shape that has been waiting all this time to happen.

This needs a lot of courage, but what we find when we do this, is beauty, bliss, a sense of deep rightness.
Those of you who were over with me in Japan last week will strongly recall how Shinzan Roshi a number of times said "You reach a point where you deeply nod". There's a deep "Yes!" in the heart.
You know, from the depths of your being, that you're leading the life that was here for you all the time.

And still, we go on.
Life is ongoing, it's a dance.
We refine even further our ability to harmonise with the truth of our lives, so this washing, this purifying of the heart carries on and carries on.
So it's the beginning of practice, but it's also the middle of practice, and the end of practice.
The endless end of practice.
There's no limit to how beautiful, how satisfying, how extraordinary your life can become.

And all of this comes out of this very simple exchange.
When you've eaten your meal, you wash your bowl.
The one essential thing that you need for success in this work, is the willingness to face yourself 100%. If you have that, your success is assured.
